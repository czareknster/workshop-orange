# What we have to do - Github Users Table

## Thanks for Your time

App is already done. Read code and learn more ;-) 

### There are some useful articles:
* https://www.youtube.com/watch?v=8aGhZQkoFbQ&t=893s
* https://www.nafrontendzie.pl/skladnia-async-await-nowy-sposob-na-promisy



## Overview
Please create a small list of developers. You could use Github API to fetch data, and other sources of your choice.
Authentication is nice to have, but without it You can make 60 requests per hour. Application should fetch profiles listed in `src/js/data/users.js` and list them by name, with projects count and sum of all earned stars.

Start write you code at this file `src/js/app.js`.

### Requirements:
* App should be created in pure JavaScript (ES6, ES7), without any additional frameworks, and work completely in browser.

### What can be helpful:
* https://developer.github.com/v3/

## Tasks
* **Server & watch:** npm run dev