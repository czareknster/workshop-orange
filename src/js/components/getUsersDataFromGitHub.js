const access_token = '15eddbb1e575663ff01c067f961b1f722afc87cc';

const users_api_url = 'https://api.github.com/users/';
const repos_per_page = 100;

// url get user name and repos count `${users_api_url}${user}?access_token=${access_token}`
// url all user repos `${users_api_url}${user.user}/repos?page=${page}&per_page=${repos_per_page}&access_token=${access_token}`


export default class GetUsersDataFromGitHub {

    constructor(htmlID = '', usersList = []) {
        this.htmlID = htmlID;
        this.usersList = usersList;
        this.usersListToRender = [];
        this._init();
    }

    _init() {
        if (typeof this.usersList === 'object' && this.usersList.length > 0) {
            this._getUsersData();
        } else {
            console.error('This is not array or array is empty');
        }
    }

    async _getUsersData() {
        const uArray = this.usersList.map(async user => {
            const response = await fetch(`${users_api_url}${user}?access_token=${access_token}`)
                .then(resp => resp.json())
                .then(resp => {
                    this.usersListToRender.push({
                        login: resp.login,
                        name: resp.name,
                        public_repos: resp.public_repos
                    });
                });
        });
        await Promise.all(uArray);
        await this._getUsersStars();

        this._render();
    }

    async _getUsersStars() {
        const userList = this.usersListToRender.map(async user => {
            await this._getUserStars(user);
        });
        await Promise.all(userList);
    }

    async _getUserStars(user) {
        const pagesList = Array.from({length: Math.ceil(user.public_repos / repos_per_page)}, (v, k) => k + 1);
        let userStars = 0;
        const uArray = pagesList.map(async page => {
            const response = await fetch(`${users_api_url}${user.login}/repos?page=${page}&per_page=${repos_per_page}&access_token=${access_token}`)
                .then(resp => resp.json())
                .then(resp => {
                    resp.forEach(repo => {
                        userStars += repo.stargazers_count;
                    })
                });
        });

        await Promise.all(uArray);
        user.stargazers_count = userStars;
    }

    _render() {
        let table = document.createElement('table');
        let html = `
            <caption>Title</caption>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Repositories</th>
                    <th>Stars</th>
                </tr>
            </thead>
            <tbody>`;

        this.usersListToRender.forEach(user => {
            html += `
            <tr>
                <td>${user.name}</td>
                <td>${user.public_repos}</td>
                <td>${user.stargazers_count}</td>
            </tr>`
        });

        html += `</tbody>`;
        table.innerHTML = html;
        table.className = 'user-list__table';

        document.getElementById(this.htmlID).appendChild(table);

    }


};