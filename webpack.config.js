const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
    entry: ['whatwg-fetch', '@babel/polyfill', './src/js/app.js'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js',
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env',
                                {
                                    targets: {
                                        browsers: [
                                            "last 5 version"
                                        ]
                                    }
                                }
                            ]
                        ]
                    }
                }
            },
            {
                test: /app.scss/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        //new UglifyJSPlugin(),
        new HtmlWebpackPlugin({
            title: 'Orange Workshop',
            minify: {
                collapseWhitespace: false
            },
            hash: true,
            template: path.resolve(__dirname, 'src/templates/page/template.html')
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 4001
    }
};